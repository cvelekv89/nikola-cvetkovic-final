export interface Contact {
    id:number,
    firstname:string,
    lastname:string,
    profilepic?:string,
    age?:number,
    birthdate?:number,
}