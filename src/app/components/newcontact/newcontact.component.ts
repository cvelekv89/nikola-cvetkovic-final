import { Component } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { ContactsModel } from "../../services/contacts.model";
import {
  IMyDpOptions,
  IMyDateModel
} from "../../../../node_modules/mydatepicker";

@Component({
  selector: "app-newcontact",
  templateUrl: "./newcontact.component.html"
})
export class NewContact {
  newContactForm: FormGroup;
  submittedAtt = false;
  contact = {};

  currYear;
  yearBirth;
  age;



  public myDatePickerOptions: IMyDpOptions = {
    dateFormat: "dd.mm.yyyy",
    openSelectorOnInputClick: true,
    editableDateField: false,
    inline: false
  };

  constructor(private contactsModel: ContactsModel, private router: Router) {
    this.newContactForm = new FormGroup({
      id: new FormControl(),
      firstName: new FormControl("", Validators.required),
      lastName: new FormControl("", Validators.required),
      profilePic: new FormControl(""),
      age: new FormControl(""),
      birthdate: new FormControl(null)
    });
  }
  addContact() {
    this.submittedAtt = true;
    if (this.contact["firstname"] && this.contact["lastname"]) {

      this.currYear = new Date().getFullYear();
      this.yearBirth = this.newContactForm.value.birthdate.jsdate.getFullYear();
      this.age = this.newContactForm.controls.age.value;

      if (
        this.newContactForm.controls.age.value &&
        this.newContactForm.controls.birthdate.value
      ) {
        if (this.currYear - this.yearBirth == this.age) {
          let contact = {
            id: this.newContactForm.controls.id.value,
            firstname: this.newContactForm.controls.firstName.value,
            lastname: this.newContactForm.controls.lastName.value,
            profilepic: this.newContactForm.controls.profilePic.value,
            age: this.newContactForm.controls.age.value,
            birthdate: this.newContactForm.value.birthdate.jsdate
            
          };
          console.log(this.contact['birthdate'])
          delete contact["id"];
          this.contactsModel.addContact(contact, () => {
            this.router.navigate(["/contacts"]);
          });
        } else {
          alert("Age is not correct");
        }
      } else if (
        this.newContactForm.controls.age.value ||
        this.newContactForm.controls.birthdate.value
      ) {
        let contact = {
          id: this.newContactForm.controls.id.value,
          firstname: this.newContactForm.controls.firstName.value,
          lastname: this.newContactForm.controls.lastName.value,
          profilepic: this.newContactForm.controls.profilePic.value,
          age: this.newContactForm.controls.age.value,
          birthdate: this.newContactForm.value.birthdate.jsdate
        };
        
        delete contact["id"];
        this.contactsModel.addContact(contact, () => {
          this.router.navigate(["/contacts"]);
        });
      } else {
        alert("Age and/or birthdate must be inputted!");
      }
    }
  }

  // onDateChanged(event: IMyDateModel) {}

  // setDate(): void {
  //   let date = new Date();
  //   this.newContactForm.patchValue({
  //     birthdate: {
  //       date: {
  //         year: date.getFullYear(),
  //         month: date.getMonth() + 1,
  //         day: date.getDate()
  //       }
  //     }
  //   });
  //   console.log(date);
  // }
  // clearDate(): void {
  //   this.newContactForm.patchValue({ birthdate: null });
  // }
}
