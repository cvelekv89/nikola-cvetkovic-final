import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';

import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from "@angular/router";
import { Home } from './components/home/home.component';
import { Contacts } from './components/contacts/contacts.component';
import { NewContact } from './components/newcontact/newcontact.component';
import { Contact } from './components/contact/contact.component';
import { EditContact } from './components/editcontact/editcontact.component';

import { ReactiveFormsModule } from '@angular/forms';

import { ContactsService } from './services/contactsservice.service';
import { ContactsModel } from './services/contacts.model';
import { Info } from './components/singlecontactroutes/info/info.component';
import { Edit } from './components/singlecontactroutes/edit/edit.component';
import { Delete } from './components/singlecontactroutes/delete/delete.component';
import { Menu } from './components/menu/menu';
import { HttpModule } from '@angular/http';
import { FilterPipe } from './filter.pipe';
import { Pipe, PipeTransform } from '@angular/core'; 
import { MyDatePickerModule } from '../../node_modules/mydatepicker';
import { dateFormatPipe } from './dateformatpipe.pipe';

const routes:Routes = [
  { path: "", component: Home },
  { path: "contacts", component: Contacts },
  { path: "contact/new", component: NewContact },
  { path: "contacts/:id", component: Contact, children: [
    { path: "", redirectTo: "info", pathMatch: "full" },
    { path: "info", component: Info },
    { path: "edit", component: Edit },
    { path: "delete", component: Delete }
  ] 
},
  { path: "contacts/:id/edit", component: EditContact }
]



@NgModule({
  declarations: [
    AppComponent,
    Home,
    Contacts,
    NewContact,
    Contact,
    EditContact,
    Info,
    Edit,
    Delete,
    Menu,
    FilterPipe,
    dateFormatPipe
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    FormsModule,
    HttpModule,
    ReactiveFormsModule,
    MyDatePickerModule
  ],
  providers: [ContactsService,ContactsModel],
  bootstrap: [AppComponent]
})
export class AppModule { }
