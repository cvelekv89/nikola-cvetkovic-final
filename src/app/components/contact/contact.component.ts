import { Component } from "@angular/core";
import { ContactsModel } from "../../services/contacts.model";
import { ActivatedRoute } from '@angular/router'

@Component({
  selector: "app-contact",
  templateUrl: "./contact.component.html"
})
export class Contact {
  id;
  firstName;
  lastName;
  age;
  birthdate;
  profilepic;
  searchText = '';

  constructor(
    public contactsModel: ContactsModel,
    private aRoute: ActivatedRoute
  ) {}
  ngOnInit(){
    
        this.aRoute.params.subscribe(({id}) => {
           this.id=id;
        });
        this.contactsModel.initializeContactsClbk(() =>{
          this.setContactsVariables();
        });
       
    }
    setContactsVariables(){
      this.contactsModel.getOneContact(this.id,({firstname,lastname,age,birthdate,profilepic})=>{
      this.firstName = firstname;
      this.lastName = lastname;
      this.age = age;
      this.birthdate = birthdate;
      this.profilepic = profilepic;
      });
    }
  
}
