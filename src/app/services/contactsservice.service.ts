import { Injectable } from "@angular/core";
import { Http } from "@angular/http";
import "rxjs/add/operator/map";


@Injectable()
export class ContactsService {
  
  constructor(private http: Http) {}

  getContactsObservable() {
    return this.http.get("http://localhost:3000/contacts").map(response => {
      return response.json();
    });
  }
  getContactObservable(id) {
    return this.http.get("http://localhost:3000/contacts/"+id).map(response => {
      return response.json();
    });
  }
  addContactObservable(contact) {
    return this.http.post("http://localhost:3000/contacts",contact).map(response => {
      return response.json();
    });
  }
  updateContactObservable(contact) {
    return this.http.patch("http://localhost:3000/contacts/"+contact.id,contact).map(response => {
      return response.json();
    });
  }

  deleteContactObservable(id){
    return this.http.delete("http://localhost:3000/contacts/"+id).map(response => {
        return response.json();
      });
  }
}
