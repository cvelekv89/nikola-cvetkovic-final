import { Injectable } from "@angular/core";
import { ContactsService } from "./contactsservice.service";
import { Contact } from "../interface/contact";

@Injectable()
export class ContactsModel {
  contacts: Contact[] = [];

  constructor(private service: ContactsService) {
    this.refreshContacts();
  }
  refreshContacts() {
    this.service.getContactsObservable().subscribe(contacts => {
      this.contacts = contacts;
    });
  }

  initializeContactsClbk(clbk) {
    if (!this.contacts || this.contacts.length < 1) {
      this.service.getContactsObservable().subscribe(contacts => {
        this.contacts = contacts;
        clbk();
      });
    } else {
      clbk();
    }
  }
  getOneContact(id, clbk) {
    for (var i = 0; i < this.contacts.length; i++) {
      if (this.contacts[i].id == id) {
        clbk(this.contacts[i]);
      }
    }
  }

  updateContacts(contact, clbk) {
    this.service.updateContactObservable(contact).subscribe(contact => {
      this.refreshContacts();
      clbk();
    });
  }

  addContact(contact, clbk) {
    this.service.addContactObservable(contact).subscribe(contact => {
      this.contacts.push(contact);
      clbk();
    });
  }
  deleteContact(id, clbk) {
    this.service.deleteContactObservable(id).subscribe(() => {
      this.refreshContacts();
      clbk();
    });
  }
}
