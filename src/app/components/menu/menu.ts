import { Component } from '@angular/core';

@Component({
    selector: 'menu-list',
    templateUrl: './menu.html'
})

export class Menu{
    links = [
        {
            name:'Home',
            path:'/',
            active: false
        },
        {
            name:'Contacts',
            path:'/contacts',
            active: false
        },
        {
            name:'Add new Contact',
            path:'/contact/new',
            active: false
        }
        
    ]
}