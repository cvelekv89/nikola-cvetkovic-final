import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { ContactsModel } from "../../../services/contacts.model";
import { Router } from "@angular/router";

@Component({
  selector: "app-info",
  templateUrl: "./info.component.html"
})
export class Info {
  id;
  firstName;
  lastName;
  age;
  birthdate;
  profilePic;

  showMessage: boolean = false;

  constructor(
    public aRoute: ActivatedRoute,
    public contactsModel: ContactsModel,
    private router: Router
  ) {}
  showMessagePopUp(){
    this.showMessage = true;
  }
  ngOnInit(){
    this.aRoute.parent.params.subscribe(({id})=>{
      this.id=id;
    });
    this.contactsModel.initializeContactsClbk(()=>{
      this.setContactsVariables();
    });
  }
  setContactsVariables(){
    this.contactsModel.getOneContact(this.id, ({firstname,lastname,age,birthdate,profilepic}) => {
      this.firstName = firstname;
      this.lastName = lastname;
      this.age = age;
      this.birthdate = birthdate;
      this.profilePic = profilepic;
      
    })
  }
  editContact() {
    this.router.navigate(["/contacts", this.id, "edit"]);
  }
  deleteContact() {
    this.showMessage=true;
    this.router.navigate(["/contacts", this.id, "delete"]);
  }

  
}
