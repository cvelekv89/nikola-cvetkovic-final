import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { ActivatedRoute } from "@angular/router";
import { ContactsModel } from "../../../services/contacts.model";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import {
  IMyDpOptions,
  IMyDateModel
} from "../../../../../node_modules/mydatepicker";

@Component({
  selector: "app-edit",
  templateUrl: "./edit.component.html"
})
export class Edit implements OnInit {
  id;
  currYear;
  yearBirth;
  ageIn;

  contact = {};
  editContactForm: FormGroup;
  submittedAtt = false;

  public myDatePickerOptions: IMyDpOptions = {
    dateFormat: "dd.mm.yyyy",
    openSelectorOnInputClick: true,
    editableDateField: false,
    inline: false
  };

  constructor(
    private router: Router,
    public aRoute: ActivatedRoute,
    public contactsModel: ContactsModel
  ) {
    this.editContactForm = new FormGroup({
      id: new FormControl({ value: "", disabled: true }),
      firstname: new FormControl({ value: "", disabled: true }),
      lastname: new FormControl({ value: "", disabled: true }),
      profilepic: new FormControl(),
      age: new FormControl(),
      birthdate: new FormControl()
    });
  }

  ngOnInit() {
    this.aRoute.parent.paramMap.subscribe(params => {
      this.id = params.get("id");

      this.contactsModel.initializeContactsClbk(() => {
        this.contactsModel.getOneContact(this.id, contactObj => {
          this.contact = contactObj;
          var date = new Date(contactObj.birthdate);

          let allValues = {
            ...this.contact,
            birthdate: {
              date: {
                day: date.getDate(),
                month: date.getMonth() + 1,
                year: date.getFullYear()
              }
            }
          };
          this.editContactForm.setValue(allValues);
        });
      });
    });
  }

  saveContact(){
    this.submittedAtt = true;

    this.currYear = new Date().getFullYear();
    this.yearBirth = this.editContactForm.value.birthdate.jsdate.getFullYear();
    this.ageIn = this.editContactForm.controls.age.value;

    if (this.editContactForm.controls.age.value && this.editContactForm.controls.birthdate.value) {
      if (this.currYear - this.yearBirth == this.ageIn) {
        let contact = {
          id: this.editContactForm.controls.id.value,
          profilepic: this.editContactForm.controls.profilepic.value,
          age: this.editContactForm.controls.age.value,
          birthdate: this.editContactForm.value.birthdate.jsdate
         };
        this.contactsModel.updateContacts(contact, () => {
          this.router.navigate(["/contacts"]);
        });
      } 
      else {
        alert("Age is not correct");
      }
    }
     else if (this.editContactForm.controls.age.value || this.editContactForm.controls.birthdate.value) {
      let contact = {
        id: this.editContactForm.controls.id.value,
        profilepic: this.editContactForm.controls.profilepic.value,
        age: this.editContactForm.controls.age.value,
        birthdate: this.editContactForm.value.birthdate.jsdate
      };

      this.contactsModel.updateContacts(contact, () => {
        this.router.navigate(["/contacts"]);
      });
    } else {
      alert("Age and/or birthdate must be inputted!");
    }
  
  }
}
 