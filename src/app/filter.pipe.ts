import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: 'filterPipe'
})
export class FilterPipe implements PipeTransform {
    transform(values: any[], searchString: any): any {
        if(!values) return [];
        if(!searchString) return values;
        return values.filter((value)=>{
          return value.firstname !== undefined && value.firstname.toLowerCase().indexOf(searchString) > -1;
      });
    
    }
}
