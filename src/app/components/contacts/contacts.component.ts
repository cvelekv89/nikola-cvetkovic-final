import { Component, OnInit } from "@angular/core";
import { ContactsModel } from "../../services/contacts.model";
import { FilterPipe } from "../../filter.pipe";
import { Router } from "@angular/router";


@Component({
  selector: "app-contacts",
  templateUrl: "./contacts.component.html"
})
export class Contacts {
  search = "";

  constructor(public contactsModel: ContactsModel, private router: Router) {}

  openContact(id) {
    this.router.navigate(["/contacts", id]);
  }

}
