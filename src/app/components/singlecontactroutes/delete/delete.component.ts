import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ContactsModel } from '../../../services/contacts.model';

@Component({
  selector: 'app-delete',
  templateUrl: './delete.component.html'
})
export class Delete {
  id;
  constructor(
    private aroute: ActivatedRoute,
    private route: Router,
    private contactsModel: ContactsModel
  ) { }

  ngOnInit() {

    this.aroute.parent.params.subscribe(params =>{
      this.id = params.id;
    });
  }
  deleteContact(){
    this.contactsModel.deleteContact(this.id, ()=>{
      this.route.navigate(['/contacts']);
    });
  }

}
